import React from "react";
import update from "immutability-helper";
import AppointmentForm from "./AppointmentForm";
import AppointmentsList from  "./AppointmentsList";


class Appointments extends React.Component {

  constructor(props){
    super(props)
    this.state = {
      appointments: this.props.appointments,
      title: 'Eric Simon',
      appt_time: 'Tommorow at 9am',
    },
    this.handleUserInput = this.handleUserInput.bind(this)
    this.handleFormSubmit = this.handleFormSubmit.bind(this)
  }


  handleUserInput(e) {
    this.setState(e);
  }


  handleFormSubmit(){
    let appointment = {title: this.state.title, appt_time: this.state.appt_time}
    $.post('/appointments',
      {appointment: appointment}
    )
      .done(function(appointment){
        this.addNewAppointment(appointment);
      }.bind(this));
  }

  addNewAppointment(appointment){
    let appointments = update(this.state.appointments, { $push: [appointment]});
    this.setState({ appointments: appointments});
  }

  render() {
    return (
      <div>
        <AppointmentForm title={this.state.title}
                         appt_time={this.state.appt_time}
                         onUserInput={this.handleUserInput}
                         onFormSubmit={this.handleFormSubmit}/>
        <AppointmentsList appointments={this.props.appointments}/>
      </div>
    )
  }
}

export default Appointments;
