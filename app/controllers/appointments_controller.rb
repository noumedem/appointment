class AppointmentsController < ApplicationController
  def index
    @appointments = MyAppointment.order('appt_time ASC')
    @appointment = MyAppointment.new
  end
  
  def create
    @appointment = MyAppointment.new(appointment_params)
    if @appointment.save
      render json: @appointment
    else
      render json: @appointment.errors, status: :unprocessable_entity
    end
  end
  
  private
  
  def appointment_params
    params.require(:appointment).permit(:title, :appt_time)
  end
end
